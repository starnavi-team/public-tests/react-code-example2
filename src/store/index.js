import { init } from '@rematch/core';
import * as models from 'store/models';

function configureStore() {
  return init({
    models,
  });
}

const store = configureStore();

export default store;
