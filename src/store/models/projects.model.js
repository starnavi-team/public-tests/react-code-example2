import { getProjects } from 'api';

export default {
  state: {
    projects: null,
  },
  effects: {
    async getProjects() {
      try {
        const { data } = await getProjects();

        if (!data) throw new Error('No data (getProjects)');

        this.setProjects(data);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    },
  },
  reducers: {
    setProjects(state, payload) {
      return {
        ...state,
        projects: payload,
      };
    },
  },
};
