const ROUTES_URLS = {
  today: '/today',
  analytic: '/analytic',
};

export default ROUTES_URLS;
