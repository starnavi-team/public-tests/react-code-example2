import styled from 'styled-components';
import Select from 'react-select';

const UISelect = styled(Select).attrs(() => ({
  classNamePrefix: 'rs',
}))`
  .${({ classNamePrefix }) => classNamePrefix} {
    &__placeholder {
        color: #000;
    }
    
    &__single-value {
      color: #000;
    }
    
    &__indicator-separator {
        display: none;
    }
    
    &__control {
      border-radius: 0;
      border-color: #000;
      box-shadow: none;
      
      &--menu-is-open {
        .rs__dropdown-indicator {
          border-width: 0 7.5px 10px 7.5px;
          border-color: transparent transparent #000 transparent;
        }
      }
      
      &:hover {
        border-color: #000;
      }
    }
    
    &__indicators {
      padding: 0 10px;
    }
    
    &__dropdown-indicator {
      width: 0;
      height: 0;
      border-style: solid;
      border-width: 10px 7.5px 0 7.5px;
      border-color: #000 transparent transparent transparent;
      padding: 0;
      transition: border .5s;
          
      svg {
        display: none;
      }
    }
  }
`;

export default UISelect;
