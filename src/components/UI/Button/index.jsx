import styled, { css } from 'styled-components';

export const buttonPrimaryCss = css`
  background: cornflowerblue;  
  border: none;
  transition: all .3s;
  transition: background  .3s, color .3s;

  &:hover {
    color: white;
    background: dodgerblue;
  }
`;

export const buttonDeleteCss = css`
  background: firebrick;
  border: none;
  transition: all .3s;
  transition: background  .3s, color .3s;
  
  &:hover {
    color: white;
    background: indianred;
  }
`;

export const Button = styled.button.attrs(props => ({
  type: props.type || 'button',
}))`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 15px;
  min-width: 80px;
  background: #fff;
  border: 1px solid #000;
  cursor: pointer;
  
  &:focus {
    outline: none;
  }
  
  ${props => props.primary && buttonPrimaryCss}
  ${props => props.delete && buttonDeleteCss}
`;
