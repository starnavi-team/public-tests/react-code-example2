import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { arrayOf, func } from 'prop-types';
import TaskItem from 'components/App/Main/Today/TasksList/TaskItem';
import { taskExtendedShape } from 'shapes';
import { createSelector } from 'reselect';

const TasksListWrap = styled.div`
  width: 100%;
  max-width: 450px;
`;

const TasksList = ({
  tasks, getTasks, getPriorities, getProjects,
}) => {
  useEffect(() => {
    getTasks();
    getPriorities();
    getProjects();
  }, []);

  return (
    <TasksListWrap>
      { tasks.map(task => (
        <TaskItem
          key={task.id}
          task={task}
        />
      ))
      }
    </TasksListWrap>
  );
};

const mapDispatchToProps = ({ tasks, priorities, projects }) => ({
  getTasks: tasks.getTasks,
  getPriorities: priorities.getPriorities,
  getProjects: projects.getProjects,
});

const tasksDataSelector = createSelector(
  ({ tasks }) => tasks.tasks,
  ({ projects }) => projects.projects,
  ({ priorities }) => priorities.priorities,
  (tasks, projects, priorities) => tasks.map(task => ({
    ...task,
    project: {
      label: projects[task.projectId].name,
      value: task.projectId,
    },
    priority: {
      label: priorities[task.priorityId].name,
      value: task.priorityId,
    },
  })),
);

const mapStateToProps = (state) => {
  const {
    projects: { projects },
    priorities: { priorities },
    tasks: { tasks },
  } = state;
  let selectedTasks = [];

  // waiting data from server
  if (projects && priorities && tasks.length) {
    selectedTasks = tasksDataSelector(state);
  }
  return {
    tasks: selectedTasks,
  };
};

TasksList.propTypes = {
  tasks: arrayOf(taskExtendedShape).isRequired,
  getTasks: func.isRequired,
  getPriorities: func.isRequired,
  getProjects: func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(TasksList);
