import React from 'react';
import styled, { css } from 'styled-components';
import { taskExtendedShape } from 'shapes';
import TaskMenu, { SettingsMenuButton } from 'components/App/Main/Today/TasksList/TaskMenu';
import { func } from 'prop-types';
import { connect } from 'react-redux';

const TaskDetails = styled.div`
  margin-left: 15px;
  width: 100%;
`;
const TaskName = styled.div`
  margin-bottom: 5px;
  font-size: 1rem;
`;

const TaskProject = styled.div`
  color: dimgrey;
  font-size: 0.875rem;
`;

const TaskStatus = styled.div`
  border-radius: 50%;
  background-color: ${({ name }) => {
    switch (name) {
      case 'high':
        return 'red';
      case 'medium':
        return 'yellow';
      case 'low':
      default:
        return 'white';
    }
  }};
  border: 1px solid dimgrey;
  width: 20px;
  min-width: 20px;
  height: 20px;
  color: yellow;
  cursor: pointer;
`;


const doneStyles = css`
  background: rgba(145,145,145,0.5);
  
  ${TaskStatus} {
    cursor: auto;
  }
  ${TaskName} {
    text-decoration: line-through;
  }
`;

const TaskWrap = styled.div`
  padding: 15px 10px;
  border-bottom: 1px solid darkgrey;
  display: flex;
  width: 100%;
  position: relative;
  transition: all .3s;
  
  ${({ done }) => done && doneStyles};
  
  ${SettingsMenuButton} {
    display: none;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 15px;
  }
  
  &:hover {
    ${SettingsMenuButton} {
      display: block;
    }
  }
`;

const TaskItem = ({ task, openCompletionTaskModal }) => (
  <TaskWrap done={task.done}>
    <TaskStatus
      name={task.priority.label}
      onClick={() => !task.done && openCompletionTaskModal(task.id)}
    />
    <TaskDetails>
      <TaskName>{ task.name }</TaskName>
      <TaskProject>{ task.project.label }</TaskProject>
      <TaskMenu task={task} />
    </TaskDetails>
  </TaskWrap>
);

TaskItem.propTypes = {
  task: taskExtendedShape.isRequired,
  openCompletionTaskModal: func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  openCompletionTaskModal: dispatch.taskModals.setCompletionTaskData,
});

export default connect(null, mapDispatchToProps)(TaskItem);
