import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import moment from 'moment';

const DateTimeWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const DateWrapper = styled.div`
  margin-right: 15px;
  font-size: 1.5rem;
  font-weight: bold;
`;

const DayInfo = styled.div`
  color: #8c8c8c;
  font-size: 0.875rem;
`;

const DayOfWeek = styled.div`
  margin: 0 0 10px;
`;

const DateTime = () => {
  const [date, setDate] = useState(Date.now());

  useEffect(() => {
    const cycle = () => setDate(Date.now());
    const timeOut = setInterval(cycle, 1000);

    return () => {
      clearInterval(timeOut);
    };
  }, []);

  const dateMoment = moment(date);

  return (
    <DateTimeWrapper>
      <DateWrapper>
        {dateMoment.format('D MMM')}
      </DateWrapper>
      <DayInfo>
        <DayOfWeek>
          {dateMoment.format('dd')}
        </DayOfWeek>
        <span>
          {dateMoment.format('HH:mm:ss')}
        </span>
      </DayInfo>
    </DateTimeWrapper>
  );
};

export default DateTime;
