import { axiosApi } from 'api/config';

export const createTask = data => axiosApi({
  method: 'POST',
  url: '/tasks.json',
  data,
});

export const editTask = ({ id, data }) => axiosApi({
  method: 'PATCH',
  url: `/tasks/${id}.json`,
  data,
});

export const deleteTask = id => axiosApi({
  method: 'DELETE',
  url: `/tasks/${id}.json`,
});

export const getTasks = () => axiosApi({
  method: 'GET',
  url: '/tasks.json',
});
